# Entity keys

An API to allow using keys on entities

- skeleton keys: override `on_use`
- skeleton keys: provide generic `entity_keys.on_skeleton_key_use` usable by entities
- keys: add `on_secondary_use`
- keys: provide generic `entity_keys.can_use_key` to check rights for key

## Skeleton Keys

To be persistent the secret of the entity **requires** to be serialized.
The entity "secret" field have to be named `key_lock_secret`

`on_skeleton_key_use(luaentity, user, newsecret)`

### Parameters
newsecret may be defined by the mod, the default is there just in case

### Return fields

1. secret: the actual key of the object
2. description: of the entity

## Keys

There is two pathes for key usage

1. Define a on_key_use for the entity
2. Handle the key case from on_righclick of the entity

When possible first way may be better

`on_key_use(self, player)`

1. self: the target entity
2. player: the player launching this action

# TODO

1. Support keyring
